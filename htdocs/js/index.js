Date.prototype.subHours = function(h) {
	this.setTime(this.getTime() - (h*60*60*1000));
	return this;
}

function initPage() {
	$('.eventPlayer').each(function() {
		var $player = $(this);
		var file = $player.data('file');
		if (file) {
			$player.append($('<a href="javascript:;" class="control play"><span class="icon"></span></a>'));
		}
	});
	$('.eventPlayer .control').on('click', function() {
		var $control = $(this);
		var playing = $control.is('.pause');
		var file = $control.closest('.eventPlayer').data('file');
		$('.eventPlayer > .control').not($control).removeClass('pause').addClass('play');
		$control.toggleClass('pause', !playing).toggleClass('play', playing);
		if (playing) {
			$control.trigger('pause', file);
		} else {
			$control.trigger('play', file);
		}
	});

	var $jp = $('<div id="jp" />').appendTo($('body'));
	$jp.wrap('<div style="position:relative;" />');
	$jp.jPlayer({
		swfPath: '/swf/Jplayer.swf',
		supplied: 'mp3,oga',
		ended: function() {
			$('.eventPlayer > .control').removeClass('pause').addClass('play');
		}
	});

	$('.eventPlayer .control').on('play', function(e, file) {
		$jp.jPlayer('setMedia', {
			mp3: 'http://100.denkmal.org/audio/' + file + '.low.mp3',
			oga: 'http://100.denkmal.org/audio/' + file + '.low.oga'
		});
		$jp.jPlayer('play');
	});
	$('.eventPlayer .control').on('pause', function(e, file) {
		$jp.jPlayer('pause');
	});

	$('.tabs').tabs();

	var isIOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/i) ? true : false);
	var isAndroid = (navigator.userAgent.match(/(android)/i) ? true : false);
	var isMobile = (isAndroid || isIOS);

	var isQrScan = ((window.location+'').indexOf('?qr') >= 0);
	var isFacebookRedirect = ((window.location+'').indexOf('?fb_action') >= 0) || ((window.location+'').indexOf('?og_add_unit_to_profile') >= 0);
	if (!isQrScan && !isFacebookRedirect && !isMobile) {
		var now = new Date();
		now = now.subHours(5);
		if (2012 == now.getFullYear() && 8 == now.getMonth() && now.getDate() >= 10 && now.getDate() <= 16) {
			var weekday = now.getDay();
			if (weekday == 0) {
				weekday = 7;
			}
			$('.tabs .day' + weekday + ' a').click();
		}
	}

	$('.downloadApp.iphone').toggle(!isAndroid).on('click', function() {
		window.location.href = 'http://itunes.apple.com/ch/app/denkmal/id556645955?mt=8&uo=4';
	});
	$('.downloadApp.android').toggle(!isIOS).on('click', function() {
		window.location.href = 'http://play.google.com/store/apps/details?id=com.hundert.jahre.denkmal.org';
	});

	$('#compilation-redeem').on('submit', function() {
		var code = $(this).find('input[name="code"]').val().split('-');
		if (code.length != 2) {
			alert("Ungültiger Gewinncode eingegeben.");
			return false;
		}
		$(this).find('input[name="id"]').val(code[0]);
		$(this).find('input[name="key"]').val(code[1]);

	});

	var degrees = 0;
	window.setInterval(function() {
		var degreesChange = 360 * Math.floor((Math.random()*2)+1);
		if (Math.random() > 0.5) {
			degreesChange *= -1;
		}
		degrees += degreesChange;
		var duration = Math.floor((Math.random()*1000)+700);
		$('.tabs > .qr > a').transition({
			perspective: '100px',
			rotateY: degrees+'deg'
		}, duration);
	}, 10000);
}

$(function() {
	initPage();
});

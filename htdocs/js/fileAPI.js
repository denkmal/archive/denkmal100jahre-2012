function getFileEntry(filename, create, callback) {
	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
	
	function gotFS(fileSystem) {
        fileSystem.root.getFile(filename, {create: create, exclusive: false}, gotFileEntry, fail);
     }
     
     function gotFileEntry(fileEntry) {
		callback(fileEntry);
     }
}

function fail(error) {
    console.log(error.code);
	return false;
}


function readFile(filename, callback) {
	function readyForRead(fileEntry) {
		fileEntry.file(gotFile, fail);
	}

	function gotFile(file){
	 	var reader = new FileReader();
		        reader.onloadend = function(evt) {
		            console.log("Read as text");
		            console.log(evt.target.result);

					callback(evt.target.result);
		        };
		 reader.readAsText(file);
	 }
	
	getFileEntry(filename, false, readyForRead);
}

function removeFile(filename) {
	function readyForRemove(fileEntry) {
		fileEntry.remove(null, fail);	
	}
	getFileEntry(filename, false, readyForRemove);
}


function writeFile(filename, content, callback) {	
	function readyForWrite(fileEntry) {
		fileEntry.createWriter(gotFileWriter, fail);	
	}
	
	function gotFileWriter(writer) {
        writer.onwriteend = function(evt) {
            writer.write("blabla");  
            writer.onwriteend = function(evt) {
                console.log("written blabla");
				callback(filename);
            };
        };
        writer.truncate(0);
    }
	
	getFileEntry(filename, true, readyForWrite);
}
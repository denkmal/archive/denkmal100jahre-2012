/*
 * Author: CM
 */
(function($) {
	$.fn.tabs = function() {
		return this.each(function() {
			var $buttonsContainer = $(this);
			var $contentContainer = $($buttonsContainer.data('target'));
			if (!$contentContainer.length) {
				return;
			}

            $('#main').bind('touchstart',function(ev) {});

			var $buttons = $buttonsContainer.find('a');
			var $contents = $contentContainer.find('> *');
			$buttons.click(function(event) {
				var $activeTab = $(this).closest('.tabs > *');
				var index = $activeTab.index();
				$activeTab.addClass('active').siblings().removeClass('active');
				var $activeTabContent = $contents.eq(index);
				$activeTabContent.addClass('active').show();
				$activeTabContent.siblings().removeClass('active').hide();
			});
			if (!$buttons.is('.active')) {
				$buttons.first().click();
			}
		});
	};
})(jQuery);

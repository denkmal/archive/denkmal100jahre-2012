<?php
header('Content-type: application/json');

error_reporting(E_ALL);

function error($msg) {
	echo json_encode(array('error' => $msg));
	exit(1);
}

$ua = $_SERVER['HTTP_USER_AGENT'];
if (false === strpos($ua, 'iPhone') && false === strpos($ua, 'iPad') && false === strpos($ua, 'Android')) {
	error('Invalid request');
}

$mysqli = new mysqli('localhost', 'denkmal100', 'j7zg2jkhga8asd3Sd3', 'denkmal100');
if ($mysqli->connect_errno) {
	error('Cannot connect to mysqld');
}

if (false === ($result = $mysqli->query('SELECT * FROM `code` WHERE `redeemStamp` IS NULL'))) {
	error('Cannot select code');
}
if (0 == $result->num_rows) {
	error('Cannot select code (#34512)');
}
$row = $result->fetch_assoc();

$address = sprintf('%u', ip2long($_SERVER['REMOTE_ADDR']));

if (70 <= $mysqli->query('SELECT 1 FROM `code` WHERE `redeemAddress`=\'' . $address . '\'')->num_rows) {
	error('Too many requests from this address (' . $address . ')');
}

$mysqli->query('UPDATE `code` SET `redeemStamp`=' . time() . ', `redeemAddress`=\'' . $address . '\' WHERE `id`=' . $row['id']);

$code = $row['key1'] . '-' . $row['key2'];

echo json_encode(array('code' => $code));
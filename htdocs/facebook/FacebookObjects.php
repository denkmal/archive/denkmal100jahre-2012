<?php
	$app_id = '439961076044547';

	$hirschcode = $_REQUEST['code'];
	
	$HirschAgora = array(
		'name' => 'Agora Bar Hirsch',
		'code' => 'Agora2109',
        'image' => 'Agora.png',
	);
	
	$HirschSommerresidenz =  array(
		'name' => 'Sommerresidenz Hirsch',
		'code' => 'Sommerresidenz7589',
        'image' => 'Sommerresidenz.png',
	);
	
	$HirschOFF = array(
		'name' => 'OFF Bar Hirsch',
		'code' => 'OFF9834',
		'image' => 'OFF.png',
	);
	
	$HirschClaramatte = array(
		'name' => 'Claramatte Hirsch',
		'code' => 'Claramatte1131',
        'image' => 'Claramatte.png',
	);
	
	$HirschPlattfon = array(
		'name' => 'Plattfon Hirsch',
		'code' => 'Plattfon9903',
        'image' => 'Plattfon.png',
	);
	$HirschLadybar = array(
		'name' => 'Lady Bar Hirsch',
		'code' => 'Ladybar8191',
        'image' => 'Ladybar.png',
	);
	$HirschSUD = array(
		'name' => 'SUD Hirsch',
		'code' => 'SUD3921',
        'image' => 'SUD.png',
	);
	$HirschKaserne = array(
		'name' => 'Kasernenhirsch',
		'code' => 'Kaserne2233',
        'image' => 'Kaserne.png',
	);
	$HirschFacebook = array(
		'name' => 'Bonushirsch',
		'code' => 'facebookhirsch3393',
        'image' => 'Facebook.png',
	);
	
	
	$Hirsche = array($HirschAgora, $HirschSommerresidenz, $HirschOFF,
		$HirschClaramatte, $HirschPlattfon, $HirschLadybar, $HirschSUD,
		$HirschKaserne, $HirschFacebook);
		
	$Hirsch = null;
	
	foreach ($Hirsche as $element) {
		if ($element['code'] == $hirschcode) {
			$Hirsch = $element;
			break;
		}
	}
	if (!$Hirsch) {
		return;
	}
?>

<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# denkmalorg: http://ogp.me/ns/fb/denkmalorg#">
 <title><?php echo $Hirsch['name'] ?></title>
	
 <meta property="fb:app_id" content="<?php echo $app_id; ?>" /> 
 <meta property="og:type"   content="denkmalorg:gewinnhirsch" /> 
 <meta property="og:url"    content="http://100.denkmal.org/facebook/FacebookObjects.php?code=<?php echo $Hirsch['code']; ?>" />
 <meta property="og:title"  content="<?php echo $Hirsch['name']; ?>" />
 <meta property="og:image"  content="http://100.denkmal.org/facebook/<?php echo $Hirsch['image']; ?>" />
 <meta http-equiv="refresh" content="0;URL='http://100.denkmal.org?qr'">
</head>
<body>
</body>
</html>
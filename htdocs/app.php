<?php
header('Content-type: application/json');

$version = 7; // LEAVE 1 DURING APPLE REVIEW!!!
$update = $_REQUEST['concertsChanged']; // holds the version number of the client's app

// returns the updated html content for the app
function getHtmlContent() {
	return <<<'EOT'
			<ul class="tabs-content events">
				<li class="qr">
					<h2>Gewinnspiel</h2>
					<span id="game"></span>
				</li>
				<li class="mo">
					<div class="location last">
						<h2>
							<a href="http://maps.google.com/maps?q=Offenburgerstrasse+59,+Basel,+Schweiz" target="_blank">OFF</a>
							<span>Mo 10.09.12, 19–23.30h</span>
						</h2>
						<span class="address">Offenburgerstrasse 59</span>
						<div class="event">
							<div class="eventPlayer" data-file="Menic - My Everyday Is A Noday Now"></div>
							<div class="eventInfo">
								<span class="time">20.30h – 22h</span>
								<div class="description">
									<span class="artists"><a href="http://www.myspace.com/MenicOnVocalsandGuitar" target="_blank">Menic</a></span>
									-
									<span class="style">Blues, folk</span>
								</div>
							</div>
						</div>
					</div>
				</li>

				<li class="di">
					<div class="location last">
						<h2>
							<a href="http://www.sud.ch/" target="_blank">SUD</a>
							<span>Di 11.09.12, 19.30–1h</span>
						</h2>
						<span class="address">Burgweg 7</span>
						<div class="event">
							<div class="eventPlayer" data-file="Mr. Blue & The Tight Groove - Make It Rain"></div>
							<div class="eventInfo">
								<span class="time">20.30h – 22h</span>
								<div class="description">
									<span class="artists"><a href="http://www.mrblue.ch/" target="_blank">Mr. Blue & The Tight Groove</a></span>
									–
									<span class="style">Blues</span>
								</div>
							</div>
						</div>
						<div class="event">
							<div class="eventPlayer"></div>
							<div class="eventInfo">
								<span class="time">22h – 1h</span>
								<div class="description">
									<span class="artists">DJ Bob Loko</span>
									–
									<span class="style">Jazz</span>
								</div>
							</div>
						</div>
					</div>
				</li>

				<li class="mi">
					<div class="location">
						<h2>
							<a href="http://www.plattfon.ch/" target="_blank">Plattfon</a>
							<span>Mi 12.09.12, 20–23h</span>
						</h2>
						<span class="address">Feldbergstrasse 48</span>
						<div class="event">
							<div class="eventPlayer" data-file="Togake pres. DVD Orchestra - Title1"></div>
							<div class="eventInfo">
								<span class="time">20.30h – 21.15h</span>
								<div class="description">
									<span class="artists"><a href="http://soundcloud.com/tokagesound" target="_blank">Togake pres. DVD Orchestra</a></span>
									–
									<span class="style">Experimental</span>
								</div>
							</div>
						</div>
						<div class="event">
							<div class="eventPlayer" data-file="UFO - Brahms II"></div>
							<div class="eventInfo">
								<span class="time">21.30h – 22.30h</span>
								<div class="description">
									<span class="artists"><a href="http://www.laseraction.ch/" target="_blank">UFO</a></span>
									–
									<span class="style">Experimental</span>
								</div>
							</div>
						</div>
					</div>

					<div class="location last">
						<h2>
							<a href="http://www.cafebaragora.org/" target="_blank">Café Bar Agora</a>
							<span>Mi 12.09.12, 23–3h</span>
						</h2>
						<span class="address">Feldbergstrasse 51</span>
						<div class="event">
							<div class="eventPlayer"></div>
							<div class="eventInfo">
								<span class="time">23h – 3h</span>
								<div class="description">
									<span class="artists"><a href="http://soundcloud.com/dj-pult-1" target="_blank">DJ Pult</a></span>
									–
									<span class="style">Experimental</span>
								</div>
							</div>
						</div>
					</div>
				</li>

				<li class="do">
					<div class="location last">
						<h2>
							<a href="http://www.cafebaragora.org/" target="_blank">Café Bar Agora</a>
							<span>Do 13.09.12, 21–3h</span>
						</h2>
						<span class="address">Feldbergstrasse 51</span>
						<div class="event">
							<div class="eventPlayer" data-file="De Høje Hæle - Ubehagelig, nederdrægtig og led"></div>
							<div class="eventInfo">
								<span class="time">21h – 22.30h</span>
								<div class="description">
									<span class="artists"><a href="http://www.myspace.com/dehjehle" target="_blank">De Høje Hæle</a></span>
									–
									<span class="style">Powerpop, punk, rock</span>
								</div>
							</div>
						</div>
						<div class="event">
							<div class="eventPlayer"></div>
							<div class="eventInfo">
								<span class="time">23h – 3h</span>
								<div class="description">
									<span class="artists">DJ Brother Julius & King Gin</span>
								</div>
							</div>
						</div>
					</div>
				</li>

				<li class="fr">
					<div class="location last">
						<h2>
							<a href="http://ladybar.fm/" target="_blank">Lady Bar</a>
							<span>Fr 14.09.12, 22–4h</span>
						</h2>
						<span class="address">Klybeckstrasse 53</span>
						<div class="event">
							<div class="eventPlayer"></div>
							<div class="eventInfo">
								<span class="time">22.30h – 23.30h</span>
								<div class="description">
									<span class="artists">Fabi</span>
									–
									<span class="style">Techno</span>
								</div>
							</div>
						</div>
						<div class="event">
							<div class="eventPlayer" data-file="Nina Simone - Black Is The Colour (Thom Nagy's Pirates from Mars ReEdit)"></div>
							<div class="eventInfo">
								<span class="time">23.30h – 4h</span>
								<div class="description">
									<span class="artists"><a href="http://soundcloud.com/herzschwester" target="_blank">Herzschwester</a>, <a href="http://soundcloud.com/thomnagy" target="_blank">Thom Nagy</a></span>
									–
									<span class="style">Techno</span>
								</div>
							</div>
						</div>
						<div class="event">
							<div class="eventPlayer" data-file="Samuel Luv - Sandcastle"></div>
							<div class="eventInfo">
								<span class="time">1h – 2h</span>
								<div class="description">
									<span class="artists"><a href="http://soundcloud.com/samuelluv" target="_blank">Samuel Luv</a></span>
									–
									<span class="style">Live deep/tech house</span>
								</div>
							</div>
						</div>
					</div>
				</li>

				<li class="sa">
					<div class="location">
						<h2>
							<a href="http://maps.google.com/maps?q=Claramatte,+Basel,+Schweiz" target="_blank">Claramatte</a>
							<span>Sa 15.09.12, 14–22h</span>
						</h2>
						<span class="address">Claramatte</span>
						<div class="event">
							<div class="eventPlayer" data-file="High-Class Robbery - Downtown Girl"></div>
							<div class="eventInfo">
								<span class="time">14.30h – 15.30h</span>
								<div class="description">
									<span class="artists"><a href="http://www.myspace.com/highclassrobbery" target="_blank">High-Class Robbery</a></span>
									–
									<span class="style">Punk</span>
								</div>
							</div>
						</div>
						<div class="event">
							<div class="eventPlayer" data-file="Take Away Caddy - Crazy World"></div>
							<div class="eventInfo">
								<span class="time">16h – 17h</span>
								<div class="description">
									<span class="artists"><a href="http://take-away-caddy.restorm.com/" target="_blank">Take Away Caddy</a></span>
									–
									<span class="style">Pop, rock</span>
								</div>
							</div>
						</div>
						<div class="event">
							<div class="eventPlayer" data-file="Penta-Tonic - Auf und davon"></div>
							<div class="eventInfo">
								<span class="time">17.30h – 18.30h</span>
								<div class="description">
									<span class="artists"><a href="http://www.facebook.com/pentatonicbasel" target="_blank">Penta-Tonic</a></span>
									–
									<span class="style">Pop, rock</span>
								</div>
							</div>
						</div>
						<div class="event">
							<div class="eventPlayer" data-file="Tom Swift - Loopomatic Groove Circulator"></div>
							<div class="eventInfo">
								<span class="time">19h – 20h</span>
								<div class="description">
									<span class="artists"><a href="http://www.myspace.com/tomswiftmusic" target="_blank">Tom Swift</a></span>
									–
									<span class="style">Funk</span>
								</div>
							</div>
						</div>
						<div class="event">
							<div class="eventPlayer" data-file="The Triad - Monster"></div>
							<div class="eventInfo">
								<span class="time">20.30h – 21.30h</span>
								<div class="description">
									<span class="artists"><a href="http://www.myspace.com/thetriadband" target="_blank">The Triad</a></span>
									–
									<span class="style">Blues, rock</span>
								</div>
							</div>
						</div>
					</div>

					<div class="location last">
						<h2>
							<a href="http://www.kaserne-basel.ch/" target="_blank">Kaserne</a>
							<span>Sa 15.09.12, 22–4h</span>
						</h2>
						<span class="address">Klybeckstrasse 1B</span>
						<div class="event">
							<div class="eventPlayer" data-file="WolfWolf - The Wolves are Coming"></div>
							<div class="eventInfo">
								<span class="time">22.30h – 23.15h</span>
								<div class="description">
									<span class="artists"><a href="http://www.wolfwolfband.com/" target="_blank">WolfWolf</a></span>
									–
									<span class="style">Garagepunk</span>
								</div>
							</div>
						</div>
						<div class="event">
							<div class="eventPlayer" data-file="Amplifier - The Wave"></div>
							<div class="eventInfo">
								<span class="time">23.30h – 1h</span>
								<div class="description">
									<span class="artists"><a href="http://www.amplifiertheband.com/" target="_blank">Amplifier</a></span>
									–
									<span class="style">Spacerock</span>
								</div>
							</div>
						</div>
						<div class="event">
							<div class="eventPlayer"></div>
							<div class="eventInfo">
								<span class="time">1h – 4h</span>
								<div class="description">
									<span class="artists"><a href="http://www.myspace.com/bitchqueensdjteam" target="_blank">Bitch Queens DJ Team</a></span>
									–
									<span class="style">Rock, trash</span>
								</div>
							</div>
						</div>
					</div>
				</li>

				<li class="so">
					<div class="location last">
						<h2>
							<a href="http://www.sommerresidenz.ch/" target="_blank">SommerResidenz</a>
							<span>So 16.09.12, 11–1h</span>
						</h2>
						<span class="address">Schwarzwaldallee 305 (NT Areal)</span>
						<div class="event">
							<div class="eventPlayer" data-file="Pink Floyd - Alan's Psychedelic Breakfast"></div>
							<div class="eventInfo">
								<span class="time">ab 11h</span>
								<div class="description">
									<span class="artists">Brunch</span>
								</div>
							</div>
						</div>
						<div class="event">
							<div class="eventPlayer"></div>
							<div class="eventInfo">
								<span class="time">14h – 20h</span>
								<div class="description">
									<span class="artists">DJ Chang Kee Jazz, DJ City of Gold</span>
									–
									<span class="style">Soul, funk, rock</span>
								</div>
							</div>
						</div>
						<div class="event">
							<div class="eventPlayer" data-file="Anja Rueegsegger - Nr. 4"></div>
							<div class="eventInfo">
								<span class="time">20h – 20.45h</span>
								<div class="description">
									<span class="artists"><a href="http://soundcloud.com/anja-rueegsegger" target="_blank">Anja Rüegsegger</a></span>
									–
									<span class="style">Singer-songwriter</span>
								</div>
							</div>
						</div>
						<div class="event">
							<div class="eventPlayer" data-file="Chevre Cho - Borino"></div>
							<div class="eventInfo">
								<span class="time">21h – 22h</span>
								<div class="description">
									<span class="artists"><a href="http://www.myspace.com/chevrecho" target="_blank">Chèvre Chô</a></span>
									–
									<span class="style">Acoustic, folk</span>
								</div>
							</div>
						</div>
						<div class="event">
							<div class="eventPlayer"></div>
							<div class="eventInfo">
								<span class="time">22h - 1h</span>
								<div class="description">
									<span class="artists">DJ Chang Kee Jazz, DJ City of Gold</span>
									–
									<span class="style">Soul, funk, rock</span>
								</div>
							</div>
						</div>
					</div>
				</li>
			</ul>
EOT;
}

if ($update > 0) {
	$response = array();

	$response['newConcerts'] = $update < $version; // true if phone needs to be updated
	$response['concertState'] = $version;

	if ($response['newConcerts']) {
		$response['concertChanges'] = getHtmlContent();
	} else {
		$respnse['concertChanges'] = '';
	}

	echo json_encode($response);
}
#!/bin/bash
set -e

MP3=$1
MP3_LOW=$(echo $MP3 | sed 's/.mp3$/.low.mp3/')
OGA_LOW=$(echo $MP3 | sed 's/.mp3$/.low.oga/')

if [ "$MP3" == "$MP3_LOW" ]; then
	echo 'ERROR: Filename does not end on ".mp3"?'
	exit 1
fi

id3v2 --delete-all "$MP3"

lame -h -V7 "$MP3" "$MP3_LOW"

mpg321 "$MP3" -w - | oggenc -q 2.5 -o "$OGA_LOW" -

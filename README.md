*100 Jahre Denkmal.org* 2012
============================

Website for the music festival *Jahre Denkmal.org* in 2012.

- **Functionality**: List of concerts for each day, contest to win a festival compilation, app download.
- **Technology**: Static HTML.

---
![Screenshot 1](docs/screenshot1.png)

---
![Screenshot 2](docs/screenshot2.png)
